#------------PART 5------------
# Napisz i przetestuj funkcję sum która:
# a) przyjmuje dwa parametry: A oraz B
# b) zwraca sumę argumentów
# c) Nic nie wypisuje
print('------------PART 5------------')
val1 = sum(5,10)
val2 = sum(7,6)
val3 = sum(val1,val2)
val4 = sum(sum(5,10),10)
print(f'Suma: {val1}')
print(f'Suma: {val2}')
print(f'Suma: {val3}')
print(f'Suma: {val4}')
print('------------------------------\n')
#
# output:
#   Suma: 15
#   Suma: 13
#   Suma: 28
#   Suma: 25