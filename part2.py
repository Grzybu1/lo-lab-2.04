#------------PART 2------------
# Napisz i przetestuj funkcję sum_and_diff która:
# a) nie przyjmuje parametrów
# b) nie zwraca wartości
# c) deklaruje wewnątrz dwie zmienne: A oraz B
# d) przypisuje do wartości odpowiednio wartości 5 oraz 10
# e) wypisuje sumę oraz różnicę tych liczb w osobnych linijkach
#
# output:
#     Suma: 15
#     Różnica: -5
#