#------------PART 7------------
# Napisz i przetestuj funkcję subt która:
# a) przyjmuje dwa parametry: A oraz B
# b) zwraca wynik odejmowania większego argumentu od mniejszego
# c) nic nie wypisuje
#
# Przykład:
#
# subt(10, 15)
# zwróci
# 5
# bo 15 - 10 = 5
#
# subt(15, 10)
# zwróci
# 5
# bo 15 - 10 = 5