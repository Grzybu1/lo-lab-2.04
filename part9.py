#------------PART 9------------
# Napisz i przetestuj funkcję sub_many która:
# a) przyjmuje dwa parametry: A, B
# b) funkcja odejmuje od większego argumentu mniejszy tyle razy ile może, póki wynik jest większy lub równy 0
# c) nic nie wypisuje
# Przykład:
#
# sub_many(10, 15)
# zwróci
# 5
# ponieważ 15 - 10 = 5
#
# sub_many(10, 55)
# zwróci
# 5
# ponieważ 55 - 10 = 45; 45 - 10 = 35; 35 - 10 = 25; 25 - 10 = 15; 15 - 10 = 5. 
# Dalej już nie, ponieważ 5 - 10 < 0