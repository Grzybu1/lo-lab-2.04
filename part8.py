#------------PART 8------------
# Napisz i przetestuj funkcję add_or_sub która:
# a) przyjmuje trzy parametry: A, B oraz is_add
# b) Jeśli is_add jest ustawione na True to zwraca wynik dodawania. 
#    Jeśli is_add jest ustawione na False to wynik odejmowania większej liczby od mniejszej.
# c) nic nie wypisuje
# Przykład:
#
# add_or_sub(10, 15, True)
# zwróci
# 25
#
# add_or_sub(10, 15, False)
# zwróci
# 5