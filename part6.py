#------------PART 6------------
# Napisz i przetestuj funkcję print_relation która:
# a) przyjmuje dwa parametry: A oraz B
# b) nic nie zwraca
# c) wypisuje podane argumenty ze znakiem mniejszości/większości/równości między nimi
#
# Przykład:
#
# print_relation(10, 15)
# zwróci
# "10 < 15"
#
# print_relation(15, 10)
# zwróci
# "15 > 10"
#
# print_relation(10, 10)
# zwróci
# "10 = 10"