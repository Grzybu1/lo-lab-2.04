#------------PART 10------------
# Napisz i przetestuj funkcję modulo która:
# a) przyjmuje dwa parametry: A, B
# b) zwróci resztę z dzielenia a/b
# c) nic nie wypisuje
# d) NIE UŻYWA operatora %
# Przykład:
#
# modulo(20, 3)
# zwróci
# 2
# ponieważ 20 / 3 = (6 z resztą 2)
#
# modulo(10, 20)
# zwróci
# 10
# ponieważ 10 / 20 = (0 z resztą 10)
